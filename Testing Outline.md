# API:
* Check for "Authorization" header, 'token [xyz]'
* Access tokens generated on startup
* If authorization fails on any route, return 403
* Endpoints for:
  * **queue:**
`" .../api/queue"`
**POST**
Only accessible by Head Chef
Accepts json, eg:
``{ "name": str,
"main ingredient": str,
"ingredients": str (csv),
"price": float,
"work required": int,
"priority": int }``
Returns json serialised version of a new Order object made using supplied data
**GET**
only accessible by chefs
returns json serialised version of the first Order object in queue

  * **orders:**
`".../api/order"`
**GET**
Only accessible by Head Chef
Returns json serialised versions of all Order objects
**PUT**
Accessible by chefs and Head Chef
Accepts json, eg
``{"id": int, "amount": int
}``
Returns json serialised version of the updated Order object
**DELETE**
Only accessible by Head Chef
Accepts json, eg
``{ "id": int }``
**POST**
Accessible by anyone
Accepts json, eg
``{ "order": string }``
Returns json serialised version of an Order object

# HeadChef:
* Only accepts order names as found in chefConfig.json, returns False if anything else is given
* Modifies the priority of orders according to what's specified ("Vodka" has priority by default)
* Can Read, Update, and Delete orders via API

# Chef:
* Polls API for new work from queue
* Updates work object via API once per second

# Order:
* Work function updates other fields based on work remaining
* Able to serialise itself as json string
