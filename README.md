# Installation:

1. Make sure postgres and pipenv are installed
2. Run ``pipenv install`` in the root directory to get dependencies
3. Set up database connection and server information in config.json
4. Run ``python app.py`` to start the server

* Edit menu and change other details in chefConfig.json (will require a restart)
