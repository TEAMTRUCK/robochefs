from flask import Flask, request, json, jsonify, Response
from base import Base, engine, Session
from waitress import serve
from chef import Robochef
from headChef import HeadChef
from orders import Order
import time
import json
from threading import Thread
import queue
import random

app = Flask(__name__)
chefConfig = json.load(open("chefConfig.json", "r"))
serverConfig = json.load(open("config.json", "r"))["server"]

q = queue.PriorityQueue()
Base.metadata.create_all(engine)
session = Session()
queueRoute = "http://"+serverConfig["host"]+":"+str(serverConfig["port"])+"/"+serverConfig["queueRoute"]
orderRoute = "http://"+serverConfig["host"]+":"+str(serverConfig["port"])+"/"+serverConfig["orderRoute"]

# Would normally make these randomly generated, this is just for easier testing
headChefSecret = 1
chefSecret = 2

headChef = HeadChef(orderRoute, queueRoute, chefConfig["menu"], chefConfig["priorities"], headChefSecret)
chefs = []
for i in range(chefConfig["chefs"]):
    chefs.append(Robochef(orderRoute, queueRoute, chefConfig["chefSpeed"], chefConfig["chefRate"], chefSecret))

for chef in chefs:
    worker = Thread(target=chef.run)
    worker.setDaemon(True)
    worker.start()

def auth(request, token):
    if "Authorization" in request.headers:
        if request.headers["Authorization"] == "token "+str(token):
            return True
    else:
        return False

@app.route(orderRoute, methods=['POST', 'PUT', 'GET', 'DELETE'])
def order():
    if request.method == "GET":
        if auth(request, headChefSecret):
            orders = session.query(Order).all()
            out = list(map(lambda o: o.asDict(), orders))
            return jsonify(out)
        else:
            return "Access denied", 403

    elif request.method == "DELETE":
        if auth(request, headChefSecret):
            data = request.get_json()
            orderId = data["id"]
            order = session.query(Order).filter(Order.id == orderId).delete()
            session.commit()
            return "Deleted"
        else:
            return "Access denied", 403

    elif request.method == "POST":
        # Offload the submitted order name to HeadChef for validation
        data = request.get_json()
        orderName = data["order"]
        tempOrder = headChef.takeOrder(orderName)

        if tempOrder:
            return "Order received."
        else:
            return "Invalid order", 400

    elif request.method == "PUT":
        if auth(request, chefSecret):
            data = request.get_json()
            orderId = data["id"]
            work = data["amount"]

            # Get the relevant order object and call its work method, then save
            order = session.query(Order).filter(Order.id == orderId).first()
            order.work(work)
            session.commit()
            return jsonify(order.asDict())
        else:
            return "Access denied", 403

@app.route(queueRoute, methods=["GET","POST"])
def orderQueue():
    if request.method == "GET":
        if auth(request, chefSecret):
            if q.empty():
                # Tell the caller to wait if the queue is empty
                return jsonify({"complete": True})
            else:
                # Return a serialised order object from the queue
                order = q.get()[1]
                response = jsonify(order.asDict())
                return response
        else:
            return "Access denied", 403

    elif request.method == "POST":
        if auth(request, headChefSecret):
            order = request.get_json()

            # Create an order, and save it to the database
            validOrder = Order(order["name"], order["main ingredient"], order["ingredients"], order["price"], order["work required"], order["priority"])
            session.add(validOrder)
            session.commit()

            # Add the order to the queue
            q.put((validOrder.priority, validOrder))

            return jsonify(validOrder.asDict())
        else:
            return "Access denied", 403

serve(app, port=serverConfig["port"], host=serverConfig["host"])
