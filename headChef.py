import time
import json
import requests

class HeadChef:
    def __init__(self, api, queue, menu, priorities, token):
        self.api = api
        self.queue = queue
        self.menu = menu
        self.priorities = priorities
        self.token = token
        self.headers ={
        "Content-Type": "application/json",
        "Authorization": "token "+str(self.token)
        }

    def validateOrder(self,name):
        if name in self.menu:
            return True
        else:
            return False

    def prioritise(self, order):
        order["priority"] = 1
        ingredients = list(map(lambda o: o.lower(), order["ingredients"]))


        for p in self.priorities:
            if p.lower() in ingredients:
                order["priority"] = 0

        return order

    def takeOrder(self, name):
        valid = self.validateOrder(name)
        if valid:
            order = self.menu[name]
            order = self.prioritise(order)
            out = json.dumps(order)
            res = requests.post(self.queue, data=out, headers=self.headers)
            return True
        else:
            return False
