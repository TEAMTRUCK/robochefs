from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import json

config = json.load(open("config.json", "r"))
dbconfig = config["database"]
dbname = dbconfig["name"]
dbuser = dbconfig["user"]
dbpw = dbconfig["pw"]
dbhost = dbconfig["host"]
dbport = str(dbconfig["port"])

engine = create_engine("postgresql://"+dbuser+":"+dbpw+"@"+dbhost+":"+dbport+"/"+dbname)
Session = sessionmaker(bind=engine)

Base = declarative_base()
