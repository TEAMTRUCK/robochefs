import requests
import time
import json
from orders import Order
from base import Session

class Robochef:
    def __init__(self, api, queue, workTime, workRate, token):
        self.api = api
        self.queue = queue
        self.workTime = workTime
        self.workRate = workRate
        self.token = token
        self.headers = {
        "Content-Type": "application/json",
        "Authorization": "token "+str(self.token)
        }


    def work(self):
        out = json.dumps({
        "id": self.currentOrder["id"],
        "amount": self.workRate})
        res = requests.put(self.api, data=out, headers=self.headers)
        self.currentOrder = res.json()

    def timedWork(self, func):
        start = time.time()
        func()
        time.sleep(self.workTime - (time.time() - start) % self.workTime)

    def findWork(self, queue):
        res = requests.get(queue, headers=self.headers)
        order = res.json()
        self.currentOrder = order

    def setOrder(self, order):
        self.currentOrder = order

    def setActive(self, active):
        self.active = active

    def run(self):
        print("Looking for work")
        self.findWork(self.queue)
        while True:
            if not self.currentOrder["complete"]:
                print("Working on: "+self.currentOrder["name"])
                self.timedWork(self.work)
            else:
                print("Looking for work")
                self.findWork(self.queue)
                time.sleep(1)
