from sqlalchemy import Column, String, Integer, DateTime, Float, Boolean
from base import Base, Session
import time
from datetime import datetime
import operator

# class CustomerOrder(Base):
#     __tablename__ = "customer_orders"
#
#     id = Column(Integer, primary_key=True)
#     name = Column(String)
#     entry_time = Column(Date)
#
#     def __init__(self, name, entry):
#         self.name = name
#         self.entry_time = entry
#
#     def asDict(self):
#         return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Order(Base):
    __tablename__ = "valid_orders"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    main_ingredient = Column(String)
    ingredients = Column(String)
    price = Column(Float)
    entry_time = Column(DateTime)
    work_remaining = Column(Integer)
    work_required = Column(Integer)
    priority = Column(Integer)
    started = Column(Boolean)
    start_time = Column(DateTime)
    complete = Column(Boolean)
    complete_time = Column(DateTime)

    def __init__(self, name, mainIng, ing, price, work, priority):
        self.name = name
        self.main_ingredient = mainIng
        self.ingredients = ing
        self.price = price
        self.entry_time = datetime.fromtimestamp(time.time())
        self.work_remaining = work
        self.work_required = work
        self.priority = priority
        self.started = False
        self.complete = False

    def __lt__(self, other):
        return operator.lt(self.priority, other.priority)

    def work(self, amount):
        if self.work_remaining > 0:
            self.started = True
            if self.start_time is None:
                self.start_time = datetime.fromtimestamp(time.time())
            self.work_remaining -= amount
            if self.work_remaining <= 0:
                self.complete = True
                self.complete_time = datetime.fromtimestamp(time.time())
                
        return self

    def setPriority(self, priority):
        self.priority = priority

    def asDict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
