import unittest
from orders import Order
import time

class TestOrder(unittest.TestCase):
    def setUp(self):
        self.order = Order("test", "testmain", "test1, test2", 123.45, 5, 0)

    def test_sets_self_to_complete_when_work_remaining_equals_zero(self):
        self.assertFalse(self.order.complete)
        self.order.work(5)
        self.assertTrue(self.order.complete)

    def test_decrements_work_remaining(self):
        self.order.work(1)
        self.assertEqual(self.order.work_remaining, 4)

    def test_sets_self_as_started_when_work_begins(self):
        self.assertFalse(self.order.started)
        self.order.work(1)
        self.assertTrue(self.order.started)

    def test_work_function_returns_updated_object(self):
        self.assertEqual(self.order.work(1), self.order)

if __name__ == '__main__':
    unittest.main()
