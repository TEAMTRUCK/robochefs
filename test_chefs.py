import unittest
import time
from chef import Robochef
from headChef import HeadChef

class Dummy:
    def work(rate):
        return True

    def commitSelf():
        return True

menu = {
    "vodka": {
        "main_ingredient": "vodka",
        "ingredients": ["vodka"],
        "price": 12.34,
        "work_required": 3
    },
    "not vodka": {
        "main_ingredient": "beans",
        "ingredients": ["beans"],
        "price": 5.67,
        "work_required": 10
    },
    "ugly case": {
        "main_ingredient": "vodka AND Beans",
        "ingredients": ["BEanS","vODKa"],
        "price": 5.67,
        "work_required": 10
    }
}

priorities = ["vodka"]

class TestChef(unittest.TestCase):
    def setUp(self):
        self.chef = Robochef("http://asdf", "http://asdf", 1, 1, 1)

    def test_loops_time_properly(self):
        start = time.time()
        def func():
            pass

        self.chef.timedWork(func)
        self.assertEqual(time.time(), start+1)

class TestHeadChef(unittest.TestCase):
    def setUp(self):
        self.head = HeadChef("http://asdf", "http://asdf", menu, priorities, 2)

    def test_rejects_invalid_orders(self):
        self.assertFalse(self.head.validateOrder("not on the menu"))
        self.assertFalse(self.head.validateOrder(123))
        self.assertFalse(self.head.validateOrder(None))
        self.assertTrue(self.head.validateOrder("vodka"))
        self.assertTrue(self.head.validateOrder("not vodka"))

    def test_give_orders_with_vodka_priority(self):
        self.assertEqual(self.head.prioritise(menu["vodka"])["priority"], 0)
        self.assertEqual(self.head.prioritise(menu["ugly case"])["priority"], 0)
        self.assertEqual(self.head.prioritise(menu["not vodka"])["priority"], 1)

if __name__ == '__main__':
    unittest.main()
